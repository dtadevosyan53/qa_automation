import time

from conftest import driver
from pages.elements_page import TextBoxPage


class TestElements:
    class TestTextBox:

        def test_fill_text_box(self, driver):
            text_box_page = TextBoxPage(driver, 'https://demoqa.com/text-box')
            text_box_page.open()
            time.sleep(5)
            text_box_page.fill_all_fields()
            time.sleep(5)

            print(text_box_page.check_filled_form())



