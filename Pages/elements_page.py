from locators.elements_page_locators import TextBoxPageLocators
from pages.base_page import BasePage
import time


class TextBoxPage(BasePage):
    locators = TextBoxPageLocators()

    def fill_all_fields(self):
        self.visible_element(self.locators.FULL_NAME).send_keys('David')
        self.visible_element(self.locators.EMAIL).send_keys('dtadevosyan53@gmail.com')
        self.visible_element(self.locators.CURRENT_ADDRESS).send_keys('Manandyan 3/1')
        self.visible_element(self.locators.PERMANENT_ADDRESS).send_keys('Kashegortsneri 207/9')
        time.sleep(5)
        self.visible_element(self.locators.SUBMIT).click()
        time.sleep(5)

    def check_filled_form(self):
        full_name = self.element_present(self.locators.CREATED_FULL_NAME).text
        email = self.element_present(self.locators.CREATED_EMAIL).text
        current_address = self.element_present(self.locators.CREATED_CURRENT_ADDRESS).text
        permanent_address = self.element_present(self.locators.CREATED_PERMANENT_ADDRESS).text
        return full_name, email, current_address, permanent_address
